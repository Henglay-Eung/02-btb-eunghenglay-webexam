import Axios from "axios"
import {GET_POSTS} from './postActionTypes'
export const getPosts=()=>{
    const innerGetPosts=async (dispatch)=>{
        const result=await Axios("https://jsonplaceholder.typicode.com/posts")
        dispatch({
           type:GET_POSTS,
           data:result.data
        })
    }
    return innerGetPosts
}
export const getPostsByID=(props)=>{
    const innerGetPostsByID=async (dispatch)=>{
        const result=await Axios(`https://jsonplaceholder.typicode.com/posts/${props.id}`)
        dispatch({
           type:GET_POSTS,
           data:result.data
        })
    }
    return innerGetPostsByID
}