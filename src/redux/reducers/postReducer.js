import {GET_POSTS} from '../actions/post/postActionTypes'
const defaultState={
    data:[]
}
export const postReducer=(state=defaultState,action)=>{
    switch(action.type){
        case GET_POSTS:
            return {
                ...state,
                data:action.data
            }
        case "GET_POSTS_BY_ID":{
            return {
                ...state,
                data:action.data
            }
        }
        default:
            return state;
    }
}