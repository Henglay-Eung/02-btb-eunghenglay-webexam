import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {getPosts} from '../redux/actions/post/postActions'
import {Link} from 'react-router-dom'
 function ListPosts(props) {
    props.getPosts()
    return (
        <div className="left-align">
            <h1>All Tutorail</h1>
            {
                props.data.map((data,index)=>{
                return <p key={index}>
                         Tutorial:{data.id}:
                         <Link to={`/tutorial/${data.id}`}>{data.title}</Link>
                        </p> 
                })
            }
        </div>
    )
}
const mapStateToProps=(state)=>{
    return {data:state.postReducer.data}
}
const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({
        getPosts,
    },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(ListPosts)