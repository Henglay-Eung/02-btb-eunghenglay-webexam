import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {getPosts,getPostsByID} from '../redux/actions/post/postActions'
import {Link} from 'react-router-dom'
 function Tutorial(props) {
    return (
        <div>
            {
                props.match.params.id
            }
        </div>
    )
}
const mapStateToProps=(state)=>{
    return {data:state.postReducer.data}
}
const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({
        getPosts,
        getPostsByID
    },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(Tutorial)